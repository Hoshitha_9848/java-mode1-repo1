package com.hcl.model;

import java.net.Inet4Address;
import java.net.UnknownHostException;

public class UserMainCode_IPAddress {
	
	public static int ipValidate(String inputString) throws  UnknownHostException {
		
		boolean valid= Inet4Address.getByName(inputString).getHostAddress().equalsIgnoreCase(inputString);
		
		if(valid==true)
			return 1;
		
		else 
			return 2;
	}
}
