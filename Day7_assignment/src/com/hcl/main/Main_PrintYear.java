package com.hcl.main;

import java.util.Calendar;
import java.util.Date;

import com.hcl.model.PrintYear;

public class Main_PrintYear {
	public static void main(String[] args) {
		Date date=new Date();
		
		Calendar calender=Calendar.getInstance();
		PrintYear.getYear(calender);
		PrintYear.getMonth(calender);
		PrintYear.getDay(calender);
		PrintYear.getHour(calender);
		PrintYear.getMinute(calender);
	
	}
}
