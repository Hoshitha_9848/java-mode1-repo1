package com.hcl.main;

import com.hcl.model.UserMainCode_GetNumberOfDays;

public class Main_GetNumberOfDays {
	public static void main(String[] args) {
		System.out.println(UserMainCode_GetNumberOfDays.getNumberOfDays(2000, 1));
		System.out.println(UserMainCode_GetNumberOfDays.getNumberOfDays(2001, 1));
		System.out.println(UserMainCode_GetNumberOfDays.getNumberOfDays(2000, 0));
	}
}
