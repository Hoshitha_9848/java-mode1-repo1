package com.hcl.main;

import java.util.Calendar;


public class MaximumValue_Main {
	public static void main(String[] args) {
	
		Calendar calender=Calendar.getInstance();
	
		System.out.println(calender.getActualMaximum(Calendar.YEAR));
		System.out.println(calender.getActualMaximum(Calendar.MONTH));
		System.out.println(calender.getActualMaximum(Calendar.WEEK_OF_MONTH));
		System.out.println(calender.getActualMaximum(Calendar.DATE));
		
	
	}
}
