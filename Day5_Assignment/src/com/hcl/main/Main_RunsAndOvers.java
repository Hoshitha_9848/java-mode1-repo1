package com.hcl.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.hcl.exception.CustomException2;
import com.hcl.model.UserMainCode_RunsAndOvers;

public class Main_RunsAndOvers {
	public static void main(String[] args) {
		BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
		int runs = 0;
		int overs = 0;
		try {
			System.out.println("Enter runs : ");
			runs = bufferedReader.read();
			System.out.println("Enter overs :");
			overs=bufferedReader.read();
			UserMainCode_RunsAndOvers.findRunRate(runs,overs);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (CustomException2 e) {
			e.printStackTrace();
		}
		
	}
}
