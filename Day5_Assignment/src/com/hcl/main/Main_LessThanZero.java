package com.hcl.main;

import java.util.Scanner;

import com.hcl.exception.MyException_2;
import com.hcl.model.MyCalculator;

public class Main_LessThanZero {
	
	public static void main(String[] args) throws MyException_2 {
		
		Scanner sc=new Scanner(System.in);
		try {
			
			System.out.println(MyCalculator.longPwer(3, 5));
			System.out.println(MyCalculator.longPwer(2, 4));
			System.out.println(MyCalculator.longPwer(0, 0));
			
		}
		catch(MyException_2 e) {
			System.out.println(e.getMessage());
		}
		finally {
			System.out.println(MyCalculator.longPwer(-1, -2));
		}
		
	}
}
