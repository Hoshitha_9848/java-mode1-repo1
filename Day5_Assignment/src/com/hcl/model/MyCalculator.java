package com.hcl.model;

import com.hcl.exception.MyException_2;

public class MyCalculator {
	
	static double result;
	
	public static double longPwer(int input1,int input2) throws MyException_2 {
		
		if((input1<0)||(input2<0)) {
			
			throw new MyException_2("input cannot be less than zero");
		}
	    if((input1==0)||(input2==0)) {
			
			throw new MyException_2("input1 or input2 cannot be zero");
		}
		
		else {
		
		result=Math.pow(input1, input2);
		
		}
		return result;
	}
}
