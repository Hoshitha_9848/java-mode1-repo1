package com.hcl.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateAndTime {

	public static void main(String[] args) {
		Date date=new Date();
		System.out.println(date);
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat();
		System.out.println(simpleDateFormat.format(date));
		Calendar calender=Calendar.getInstance();
		System.out.println("the current is : "+calender.getTime());
	}
}