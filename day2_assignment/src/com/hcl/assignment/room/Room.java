package com.hcl.assignment.room;

public class Room {
	private int roomNumber=303;
	private String roomType="Single room";
	private String roomArea="koyembed";
	private String acMachine="available";
	
	public void setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
	}
	
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	
	public void setRoomArea(String roomArea) {
		this.roomArea = roomArea;
	}
	
	public void setAcMachine(String acMachine) {
		this.acMachine = acMachine;
	}
	
	public void display_roomNumber() {
		System.out.println(roomNumber);
	}
	public void display_roomArea() {
		System.out.println(roomArea);
	}
	public void display_roomType() {
		System.out.println(roomType);
	}
	public void display_acMachine() {
		System.out.println(acMachine);
	}
}
