package com.hcl.assignment.room;

public class RoomMain {
	public static void main(String[] args) {
		Room room=new Room();
		room.setRoomNumber(202);
		room.setRoomType("triple sharing"); room.setRoomArea("Shollinganallur");
		room.setAcMachine("Not available");
		room.display_roomNumber();
		room.display_roomType();
		room.display_roomArea();
		room.display_acMachine();
	}
}
