package com.hcl.assignment.number_of_vowels;

public class VowelsCount {
	int countOfVowels=0;
	int countOfVowels(String inputString) {
		for (int i = 0; i < inputString.length(); i++) {
			char character=inputString.charAt(i);
			if ((character=='a')||(character=='e')||(character=='i')||(character=='o')||(character=='u')||(character=='A')||(character=='E')||(character=='I')||(character=='O')||(character=='U')) {
				countOfVowels++;
			}
		}
		return countOfVowels;
	}
}
