package com.hcl.assignment.inheritance;

public class A {
	
	static final String firstName="Hoshi";
	static String lastName="Dasari";
	private long phoneNumber=99999 ;
	int doorNumber=198;
	String area;
	
	public long getPhoneNumber() {
		return phoneNumber;
	}
	
	public void display_name() {
		System.out.println(firstName);
	}
	
	public int door_Number(){
		return doorNumber;
	} 
	
}
