package com.hcl.assignment.inheritance;

public class B extends A {
	
	public static void display_myFirstName() {
		System.out.println("pinky");
	}
	public static void main(String[] args) {
		A a=new A();
		System.out.println(a.firstName);
		System.out.println(a.lastName);
		System.out.println(a.door_Number());
		
		B b=new B();
		b.display_name();
		System.out.println(b.getPhoneNumber());
		System.out.println(b.door_Number());
		b.display_myFirstName();
		
	}
	
}
