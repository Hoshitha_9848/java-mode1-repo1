package com.hcl.assignment.middle_char_of_string;

public class MiddleCharacter {
	
	MiddleCharacter(String inputString) {
		int i=inputString.length();
		System.out.println((i%2)!=0 ? inputString.charAt((i/2)) : inputString.substring((i/2-1),(i/2+1) ));
	}
}
