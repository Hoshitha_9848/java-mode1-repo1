package ascii_value;

public class ASCII {
	public static void main(String[] args) {
		
		char character1='a';
		char character2='A';
		
		int asciiValue1=character1;
		int asciiValue2=character2;
		
		System.out.println("ASCII value of 'a' is "+asciiValue1);
		System.out.println("ASCII value of 'A' is "+asciiValue2);
		
	}
}
