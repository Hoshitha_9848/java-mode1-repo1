package sum_of_squares;

public class UserMainCode {
	
	public static int sumOfSquaresOfEvenDigits(int number) {
		 int sumOfSquares=0;
		while(number!=0) {
			int digit=number%10;
			if((digit%2)==0) {
				int square=digit*digit;
				sumOfSquares+=square;
				}
			number=number/10;
		}
		return sumOfSquares;
	}
}
