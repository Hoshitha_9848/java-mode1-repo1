package sum_of_odd_digits;

public class UserMainCode {
	static int oddDigitSum=0;
	public static int checkSum(int number) {
		
		while(number!=0) {
			int digit=(number%10);
			if((digit%2)==1){
				oddDigitSum+=digit;
			}
			number=number/10;
		}
		if((oddDigitSum%2)==1){
			return 1;
		}
		else {
			return -1;
		}
		
	}
	
}
