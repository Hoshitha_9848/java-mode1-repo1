package sum_of_odd_digits;

public class Main {
	public static void main(String[] args) {
		int returnOutput=UserMainCode.checkSum(231456);
		System.out.println((returnOutput>0)?"Sum of odd digits is odd" : "Sum of odd digits is even");
		
		/*
		 * if( returnOutput==1) { System.out.println("Sum of odd digits is odd"); } if(
		 * returnOutput==-1) { System.out.println("Sum of odd digits is even"); }
		 */
	}
}
