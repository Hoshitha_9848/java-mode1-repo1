package largest_word;

public class UserMainCode {
	
	String getLargestWord(String largeSentance) {
		
		String s1[]=largeSentance.split(" ");
		String reverseSentance=" ";
		
		for(int i=s1.length-1;i>=0;i--) {
			reverseSentance=reverseSentance+s1[i]+" "; 
		}
		
		String[] words=reverseSentance.split(" ");
		String largestWordOfLine=" ";
		
		for (String largestWord : words) {
			if (largestWordOfLine.length()<=largestWord.length()) {
				
				largestWordOfLine=largestWord;
			}
		}
	
		return largestWordOfLine;
	}
}
