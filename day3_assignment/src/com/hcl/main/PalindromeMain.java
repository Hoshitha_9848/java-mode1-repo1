package com.hcl.main;

import com.hcl.model.Palindrome;

public class PalindromeMain {
	public static void main(String[] args) {
	      System.out.println(Palindrome.palindromeCheck("madam"));
	      System.out.println(Palindrome.palindromeCheck("mad"));
	      System.out.println(Palindrome.palindromeCheck(" "));
	}
}
