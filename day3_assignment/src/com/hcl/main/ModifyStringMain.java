package com.hcl.main;

import com.hcl.model.UserMainCode;

public class ModifyStringMain {
	public static void main(String[] args) {
		System.out.println(UserMainCode.getString(new StringBuilder("hello")));
		System.out.println(UserMainCode.getString(new StringBuilder("kavya")));
		System.out.println(UserMainCode.getString(new StringBuilder("abq")));
		
	}
}
