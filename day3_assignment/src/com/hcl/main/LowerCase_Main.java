package com.hcl.main;

import com.hcl.model.LowerCase;

public class LowerCase_Main {
	
	public static void main(String[] args) {
		System.out.println("After conversion to lowercase: "+LowerCase.convertToLowerCase("JAVA MODE1"));
		System.out.println("After repacement of 'd' with 'h': "+ LowerCase.replaceAll("dog is god"));
	}
}
