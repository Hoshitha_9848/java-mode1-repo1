package com.hcl.main;

import com.hcl.model.SortingAnIntegerArray;

public class SortingMain {
	public static void main(String[] args) {
		
		int[] integerArray=new int [] {2,9,5,8,1,0,6,4,30,60};
		SortingAnIntegerArray.sorting(integerArray);
		SortingAnIntegerArray.findElement(integerArray);
	}
}
