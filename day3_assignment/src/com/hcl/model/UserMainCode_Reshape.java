package com.hcl.model;

public class UserMainCode_Reshape {
	public static String reshape(String inputString, char ch) {
		
		StringBuilder sb=new StringBuilder(inputString);
		sb.reverse();
		for (int i = 1;i<sb.length(); ) {
			sb.insert(i, ch);
			i=i+2;
		}
		
		return sb.toString();
	}
}
