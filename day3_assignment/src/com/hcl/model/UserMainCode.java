package com.hcl.model;

public class UserMainCode {
	public static StringBuilder getString(StringBuilder inputString) {
		
		char firstCharacter=inputString.charAt(0);		
		char secondCharacter=inputString.charAt(1);	
		
		if (firstCharacter=='k') {
			inputString.deleteCharAt(1);
		}
		else if(secondCharacter=='b') {
			inputString.deleteCharAt(0);
			
		}
		
		else inputString.delete(0, 2);
		
		return inputString;
	}
}
