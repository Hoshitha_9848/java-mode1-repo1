package com.hcl.model;

public class LowerCase {
		public static String convertToLowerCase(String inputString) {
			return inputString.toLowerCase();
		}
		
		public static String replaceAll(String inPut1) {
			return inPut1.replace('d', 'h');
		}
}
