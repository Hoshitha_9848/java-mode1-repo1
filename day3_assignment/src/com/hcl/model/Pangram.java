package com.hcl.model;

public class Pangram {
	public static void alphabetsMethod(String inputString) {
	
		if(inputString!=null) {	
	
			boolean[] alphabetsOfSentance=new boolean[26];
			int index=0;
			int flag=1;
		
			for(int i=0;i<inputString.length();i++) {
				if(inputString.charAt(i)>='A'&& inputString.charAt(i)<='Z') {
					index=inputString.charAt(i)-'A';
				}
				else if(inputString.charAt(i)>='a'&& inputString.charAt(i)<='z') {
					index=inputString.charAt(i)-'a';
				}
				alphabetsOfSentance[index]=true;
			}
		
			for(int i=0;i<25;i++) {
				if(alphabetsOfSentance[i]==false)
					flag=0;
			}
		
			System.out.println("String : "+inputString);
		
			if(flag==1)
				System.out.println("given string is a pangram");
			else
				System.out.println("not a pangram");
	
		}
		/*if(inputString==null)
			System.out.println("Enter valid input");*/
	}
	
}
