package com.hcl.model;

public class Palindrome {

	public static String palindromeCheck(String inputString) {
		String reverseString="";
		char[] c=inputString.toCharArray();
		for (int i = c.length-1; i>=0; i--) {
			reverseString+=c[i];
		}
		 if (inputString.equals(reverseString)) {
			return "yes";
		} else {
			return "no";
		}
	}
	
}
