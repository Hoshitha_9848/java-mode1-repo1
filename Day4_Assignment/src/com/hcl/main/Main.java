package com.hcl.main;

import java.util.Scanner;

import com.hcl.model.Circle;
import com.hcl.model.Rectangle;
import com.hcl.model.Square;

public class Main {
	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		
		/*Rectangle rectangle=new Rectangle("rectangle",3,4); 
		Circle circle=new Circle("circle",5);
		Square square=new Square("square", 7);
		System.out.println("Area of rectangle is: " +rectangle.calculateArea());
		System.out.println("Area of Circle is: "+circle.calculateArea());
		System.out.println("Area of square is: "+square.calculateArea());*/
		
		Rectangle rectangle= new Rectangle();
		System.out.println("Enter length and breadth of rectangle : ");
		rectangle.setLength(sc.nextInt());
		rectangle.setBreadth(sc.nextInt());
		System.out.println("the area of rectangle : ");
		System.out.println(rectangle.calculateArea());
		
		sc=null;
	}
}
