package com.hcl.main;

import com.hcl.model.A;

public class OopsExercise {
	public static void main(String[] args) {
		A objA=new A();
		System.out.println("in main():");
		//System.out.println("objA.a="+objA.a);   //'a' is abstract variable in class A. We cannot access it directly.
		System.out.println("objA.a= "+objA.getA()); //to rectify that, we need to use getters and setters
		objA.setA(222);
	}
}
