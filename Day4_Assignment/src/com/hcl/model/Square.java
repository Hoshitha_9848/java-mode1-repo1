package com.hcl.model;

public class Square extends Shape {
	
	private int side;
	
	public Square(String name, int side){
		this.side=side;
		this.calculateArea();
	}
	@Override
	
	public float calculateArea() {
		// TODO Auto-generated method stub
		return side*side;
	}
	
	public int getSide() {
		return side;
	}
	
	public void setSide(int side) {
		this.side = side;
	}
	

}
